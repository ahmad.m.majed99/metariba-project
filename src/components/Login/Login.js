import { GoogleLogin } from "react-google-login";
import { useNavigate } from "react-router-dom";

const client_id =
  "778513930561-lp2v2hq2taq7m47bcocq6el2pa1jkkcq.apps.googleusercontent.com";

function Login() {
  const navigate = useNavigate();

  const onSuccess = (res) => {
    navigate("authorpage");
    console.log("LOGIN SUCCESS! current user: ", res.profileObj);
  };

  const onFailure = (res) => {
    console.log("LOGIN FAILD ! res: ", res);
  };

  return (
    <div>
      <GoogleLogin
        clientId={client_id}
        buttonText="Login"
        onSuccess={onSuccess}
        onFailure={onFailure}
        cookiePolicy={"single_host_origin"}
        isSignedIn={true}
      />
    </div>
  );
}

export default Login;
