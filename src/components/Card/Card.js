import React, { useState } from "react";

const Card = ({ book, query }) => {
  return (
    <>
      {book
        .sort(function (a, b) {
          return (
            new Date(b.volumeInfo.publishedDate) -
            new Date(a.volumeInfo.publishedDate)
          );
        })
        .filter((post) => {
          if (query === "") {
            return post;
          } else {
            let flag = false;
            let Data = post.volumeInfo.authors;
            if (Array.isArray(Data)) {
              for (let i = 0; i < Data.length; i++) {
                if (Data[i].toLowerCase().includes(query.toLowerCase())) {
                  flag = true;
                  break;
                }
              }
              if (flag === true) {
                return post;
              }
            }
          }
          {
            return 0;
          }
        })
        .map((item, index) => {
          let thumbnail =
            item.volumeInfo.imageLinks &&
            item.volumeInfo.imageLinks.smallThumbnail;
          {
            return (
              <>
                <div className="card-container" key={index} item={book.id}>
                  <div className="image-container">
                    <img src={thumbnail} alt="book" />
                  </div>

                  <div className="card-content">
                    <div className="card-title">{item.volumeInfo.title}</div>
                    <div className="author">{item.volumeInfo.authors}</div>
                    <div className="dates">18/2/2222</div>
                    <div className="rating">5 starts</div>
                  </div>
                </div>
              </>
            );
          }
        })}
    </>
  );
};

export default Card;
