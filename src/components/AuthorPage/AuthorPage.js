import axios from "axios";
import React, { useEffect, useState } from "react";
// import Loader from "../Loader/Loader";
import LogoutButton from "../Logout/Logout";
import Card from "../Card/Card";
import "./AuthorPage.css";

const AuthorPage = () => {
  // const [error, setError] = useState(null);
  const [query, setQuery] = useState("");
  const [bookData, SetBooks] = useState([]);
  //   const [isLoaded, setIsLoaded] = useState(false);

  const GetData = async () => {
    // if (EventTarget.key === "Enter") {
    const response = await axios.get(
      `https://www.googleapis.com/books/v1/volumes?q=flowers&filter=free-ebooks&key=
          AIzaSyBw4gQzEaEHTA-Bv6JhOVJpBUnfI1UYRCU`
    );

    //   setIsLoaded(true);
    SetBooks(response.data.items);

    console.log(response.data.items[0].volumeInfo.authors);
    // } catch {
    //   (error) => {
    //     // setIsLoaded(true);
    //     console.log(error);
    //   };
    // }
    // }
  };

  useEffect(() => {
    GetData();
  }, []);

  //   if (error) {
  //     return <>{error.message}</>;
  //   } else if (!isLoaded) {
  //     return <Loader />;
  //   } else {
  return (
    <>
      <div className="nav-bar">
        <div className="logo">
          <img
            src="./images/Author_Logo_Pen_Writing-removebg-preview.png"
            alt="logo-books"
          />
        </div>
        <div className="search-bar">
          <input
            type="text"
            placeholder="Search for an author .  .  ."
            // value={query}
            onChange={(event) => setQuery(event.target.value)}
            // onKeyPress={AuthorPage}
          />
          <i class="fa-solid fa-magnifying-glass"></i>
        </div>
        <div className="logout">
          <LogoutButton />
        </div>
      </div>
      <div className="banner">
        <div className="banner-title">
          <h1>When in doubt go to the library . . .</h1>
        </div>
        <div className="banner-image">
          <img src="./images/4247528-removebg-preview.png" alt="books" />
        </div>
      </div>
      <div className="card-wrapper">
        {<Card book={bookData} query={query} />}
      </div>
    </>
  );
  //   }
};

export default AuthorPage;
